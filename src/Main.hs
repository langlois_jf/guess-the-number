module Main (main) where

import System.Random (getStdRandom, randomR)

main :: IO ()
main = do
	number <- getStdRandom $ randomR (1,20)
	putStrLn "Guess the number between 1 and 20: "
	doGuess number
        main

doGuess :: Int -> IO ()
doGuess number = do
	guess <- getLine
	if read guess == number
		then putStrLn "You got it!"
		else do
			putStrLn "Nope!"
			doGuess number
